<?php

use Ragod\ContactForm\Http\Controllers\ContactFormController;

Route::middleware('web')->controller(ContactFormController::class)->name('contact.')->group(function () {
    Route::get('/contact', 'index');
    Route::post('/contact', 'store')->name('store');
});

?>
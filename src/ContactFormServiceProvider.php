<?php

namespace Ragod\ContactForm;

use Illuminate\Support\ServiceProvider;
// Requests
use Ragod\ContactForm\Http\Requests\ContactFormRequest;
// Controllers
use Ragod\ContactForm\Http\Controllers\ContactFormController;
// Models
use Ragod\ContactForm\Models\Contact;

class ContactFormServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make(Contact::class);
        $this->app->make(ContactFormRequest::class);
        $this->app->make(ContactFormController::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/routes/web.php');
        $this->loadViewsFrom(__DIR__.'/resources/views', 'contact_form');
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');

        $this->publishes([
            __DIR__.'/resources/views' => resource_path('views/vendor/contact_form')
        ]);
    }
}

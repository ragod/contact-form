<?php

namespace Ragod\ContactForm\Http\Controllers;

use App\Http\Controllers\Controller;
use Ragod\ContactForm\Http\Requests\ContactFormRequest;
use Ragod\ContactForm\Models\Contact;

class ContactFormController extends Controller
{
    /**
     * Show contact form
     */
    public function index() {
        return view('contact_form::index');
    }
    
    /**
     * Save contact form
     */
    public function store(ContactFormRequest $request) {
        $validated = $request->validated();
        Contact::create($validated);
        return back()->with('status', 'Contact form submit successfully !');
    }
}

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Contact Form</title>
    <style>
        .contact-form-container {
            text-align: center;
            width: 50%;
        }

        input[type="text"], input[type="email"] {
            display: block;
            width: 100%;
            height: calc(0.5em + 0.75rem + 2px);
            padding: 0.375rem 0.75rem;
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            color: #495057;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            border-radius: 0.25rem;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
        }

        .form-combined {
            display: flex;
            justify-content: space-between;
        }

        .form-combined .form-data {
            width:46%;
        }

        .form-data {
            margin-bottom: 1rem;
        }

        textarea {
            display: block;
            width: 100%;
            padding: 0.375rem 0.75rem;
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            color: #495057;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            border-radius: 0.25rem;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
        }

        input[type="submit"] {
            font-size: 14px;
            line-height: normal;
            border-width: 1px;
            border-style: solid;
            width: auto;
            padding: 6px 18px;
            cursor: pointer;
            min-width: 100px;
            display: inline-block;
            border-radius: 40px;
            text-align: center;
            font-weight: normal;
            font-style: normal;
            outline: none !important;
            text-decoration: none;
            -webkit-transition: all 0.5s ease;
            -moz-transition: all 0.5s ease;
            transition: all 0.5s ease;
            background: #0556a7;
            border-color: #0556a7;
            color: #fff;
        }

        .error {
            color: rgb(179, 0, 0);
        }

        .status {
            background-color: rgb(68, 55, 252);
            color: #fff;
            width: 100%;
            padding: 0.375rem 0.75rem;
            margin-bottom: 1rem;
        }
    </style>
</head>
<body>
    <div class="contact-form-container">
        <h1>Contact Form</h1>
        @if (session('status'))
            <div class="status">{{ session('status') }}</div>
        @endif
        <form action="{{ route('contact.store') }}" method="post">
            @csrf
            <div class="form-combined">
                <div class="form-data">
                    <input type="text" name="name" id="name" placeholder="Name">
                    @error('name')
                        <span class="error">{{ $message }}</span>
                    @enderror
                </div>
                <div class="form-data">
                    <input type="email" name="email" id="email" placeholder="Email">
                    @error('email')
                        <span class="error">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-data">
                <input type="text" name="subject" id="subject" placeholder="Subject">
                @error('subject')
                    <span class="error">{{ $message }}</span>
                @enderror
            </div>
            <div class="form-data">
                <textarea name="comment" id="comment" cols="30" rows="10" placeholder="Your comment..."></textarea>
                @error('comment')
                    <span class="error">{{ $message }}</span>
                @enderror
            </div>
            <input type="submit" value="Submit">
        </form>
    </div>
</body>
</html>